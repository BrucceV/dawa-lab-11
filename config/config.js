
//Puerto
process.env.PORT = process.env.PORT || 3000;

//Entorno
process.env.NODE_ENV = process.env.NODE_ENV || 'gaaa'

//Vencimiento de Token
//60 seg * 60 min * 24 h * 30 d
process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30

//Seed de autentificacion
process.env.SEED = process. env.SEED || 'este-es-es-seed-desarrollo'

//Google Client ID
process.env.CLIENT_ID = process.env.CLIENT_ID || '473202198889-k3nb2gsofsgao20jciat4ljr1tusoto8.apps.googleusercontent.com'

//Base de Datos
let urlDB;

if(process.env.NODE_ENV === 'dev' ){
    urlDB = 'mongodb://localhost:27017/lab08'
}else{
    urlDB = 'mongodb+srv://brucce:UtTmjKg8p4.ifZt@cluster0-jp56b.mongodb.net/lab08'
}

process.env.URLDB = urlDB